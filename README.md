Rust
=================
Overview
---------------
The Rust programming language.

URLs
---------------
* https://doc.rust-lang.org/
* https://www.rust-lang.org/tools/install
* https://doc.rust-lang.org/rust-by-example/hello.html

Install
---------------
```shell
docker run -it -h rust --name rust centos
yum install -y gcc

# for the stable compiler (default)...
curl https://sh.rustup.rs -sSf |bash
export PATH=$PATH:$HOME/.cargo/bin
rustc --version
rustup update

# for the experimental compiler...
curl https://sh.rustup.rs -sSf |sh -s -- --default-toolchain=nightly
export PATH=$PATH:$HOME/.cargo/bin
rustc --version
rustup update
```

Compile
---------------
```shell
cat <<'EOF' >/tmp/main.rs
fn main()
{
	println!("Hello World.");
}
EOF
rustc /tmp/main.rs -o /tmp/main
/tmp/main
```

Build (with external dependencies)
---------------
```shell
# cargo is the rust build system that handles external library dependencies
# eg
cat <<EOF >Cargo.toml
[package]
name = "hello_world"
version = "0.1.0"
authors = ["Kevin D.Hall <kevin@khallware.com>"]

[dependencies]
time = "0.1.12"
regex = "0.1.41"
EOF
cargo build
cargo update
cargo run
```
