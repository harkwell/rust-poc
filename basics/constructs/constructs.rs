fn main()
{
	println!("if statements provide {} execution",
		if true {"conditional"} else {""});
	let mut flag = false;

	while !flag {
		println!("while loops iteratates until a condition is met");
		flag = true;
	}
	println!("for loops iterate");

	for x in 1..3 {
		println!("\tand iterate ({})", x);
	}
	let token = "foo";

	match token {
		"foo" => println!("match matches what you select: {}", token),
		_ => println!("matches by default")
	}
}
