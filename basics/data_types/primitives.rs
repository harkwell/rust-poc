use std::mem;

fn main()
{
	let grid = [
		["i8: signed int",    &(mem::size_of::<i8>().to_string())    ],
		["i16: signed int",   &(mem::size_of::<i16>().to_string())   ],
		["i32: signed int",   &(mem::size_of::<i32>().to_string())   ],
		["i64: signed int",   &(mem::size_of::<i64>().to_string())   ],
		["i128: signed int",  &(mem::size_of::<i128>().to_string())  ],
		["isize: signed int", &(mem::size_of::<isize>().to_string()) ],
		["u8: unsigned int",  &(mem::size_of::<i8>().to_string())    ],
		["u16: unsigned int", &(mem::size_of::<i16>().to_string())   ],
		["u32: unsigned int", &(mem::size_of::<i32>().to_string())   ],
		["u64: unsigned int", &(mem::size_of::<i64>().to_string())   ],
		["u128: unsigned int",&(mem::size_of::<i128>().to_string())  ],
		["usize: unsigned int",&(mem::size_of::<isize>().to_string())],
		["f32: floating point",&(mem::size_of::<f32>().to_string())  ],
		["f64: floating point",&(mem::size_of::<f64>().to_string())  ],
		["i16: signed int",   &(mem::size_of::<i16>().to_string())   ],
		["char: unicode",     &(mem::size_of::<char>().to_string())  ],
		["bool: true/false",  &(mem::size_of::<bool>().to_string())  ]
//		["unit: empty tuple", &(mem::size_of::<unit>().to_string())  ]
	];
	for row in grid.iter() {
		println!("{}s are {} bytes in size.", row[0], row[1]);
	}
}
